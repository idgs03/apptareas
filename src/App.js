import "./App.css";
import { Contador } from './components/Contador/Contador';
import { Buscador } from './components/Buscador/Buscador';
import { Lista } from './components/Lista/Lista';
import { Item } from './components/Item/Item';
import { Boton } from './components/Boton/Boton';
import React, { useState } from 'react'

function App() {
  //Estado

  const localStoragePers = localStorage.getItem('TAREAS_v1');
  let personas;

  if (!localStoragePers) {
    localStorage.setItem('TAREAS_v1', JSON.stringify([]));
    personas = [];
  } else {
    personas = JSON.parse(localStoragePers);
  }

  //Contador
  //Estado
  const [tareas, setTareas] = React.useState(personas);

  //CONTADOR
  const [buscandoValor, setBuscandoValor] = React.useState('');

  //Varibles que almacena las tareas completadas
  const compledtTareas = tareas.filter((tarea) => !!tarea.completed).length;
  const totalTareas = tareas.length;

  let buscarTareas = [];


  //Condicional
  if (!buscandoValor.length >= 1) {
    buscarTareas = tareas;
  } else {
    buscarTareas = tareas.filter((tareas) => {
      const tareaText = tareas.text.toLowerCase();
      const buscarText = buscandoValor.toLowerCase();

      return tareaText.includes(buscarText);
    });
  }

  //Funcion para actualizar localStorage
  const saveTareas = (newTareas) => {
    const stringTareas = JSON.stringify(newTareas);
    localStorage.setItem('TAREAS_v1', stringTareas);
    setTareas(newTareas);
  }

  const addTarea = (text) => {
    const newTareas = [...tareas];
    newTareas.push({
      completed: false,
      text,
    });
    saveTareas(newTareas);
  };

  const completarTarea = (text) => {
    const tareaIndex = tareas.findIndex((tarea) => tarea.text === text);
    const newTareas = [...tareas];
    newTareas[tareaIndex].completed = true;
    saveTareas(newTareas);
  };

  const deleteTarea = (text) => {
    const tareaIndex = tareas.findIndex((tarea) => tarea.text === text);
    const newTareas = [...tareas];
    newTareas.splice(tareaIndex, 1);
    saveTareas(newTareas);
  };

  return (
    <div className="App">
      <div className="container">
        <div className="row">
          <div className="col-2"></div>
          <div className="col-8 App-header">
            <Contador total={totalTareas} completed={compledtTareas} />
            <br />
            <Boton addTarea={addTarea} />
            <br />
            <div className="row">
              <Buscador buscandoValor={buscandoValor} setBuscandoValor={setBuscandoValor} />
              <Lista>
                {buscarTareas.map((tarea) => (
                  <Item
                    text={tarea.text}
                    key={tarea.text}
                    completed={tarea.completed}
                    onComplete={() => completarTarea(tarea.text)}
                    onDelete={() => deleteTarea(tarea.text)}
                  />
                ))}
              </Lista>
            </div>
          </div>
          <div className="col-2"></div>
        </div>

      </div>
    </div>
  );
}

export default App;
