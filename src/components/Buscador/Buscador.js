import React, { useState } from 'react'

function Buscador({ buscandoValor, setBuscandoValor }) {

    const Buscar = (event) => {
        setBuscandoValor(event.target.value)
    }

    return (
        <>
            <div className="input-group input-group-sm mb-3">
                <input type="text" className="form-control buscador" placeholder="Buscar tarea" aria-label="Buscar tarea" onChange={Buscar} aria-describedby="Buscar tarea" />
            </div>

        </>
    )
}

export { Buscador }