import React, { useState } from 'react';
import Modal from 'react-modal';

const customStyles = {
    overlay: {
        position: 'fixed',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: 'rgba(255, 255, 255, 0.75)'
    },
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        background: '#A39A9C',
        transform: 'translate(-50%, -50%)',
    },
};

function Boton(props) {

    let subtitle;
    const [modalIsOpen, setIsOpen] = useState(false);

    function openModal() {
        setIsOpen(true);
    }

    function afterOpenModal() {
        // references are now sync'd and can be accessed.
        subtitle.style.color = '#000';
    }

    function closeModal() {
        setIsOpen(false);
    }

    const [newTareaValue, setNewTareValue] = useState('');

    const onChange = (event) => {
        setNewTareValue(event.target.value);
    };

    const onSubmit = (event) => {
        event.preventDefault();
        closeModal();
        props.addTarea(newTareaValue);
    };

    return (
        <div>

            <button
                type="button"
                className='btn btn-outline-success btn-sm'
                onClick={openModal}
            >
                Agregar tarea
            </button>

            <Modal
                isOpen={modalIsOpen}
                onAfterOpen={afterOpenModal}
                onRequestClose={closeModal}
                style={customStyles}
                contentLabel="Modal"
            >
                <h2 ref={(_subtitle) => (subtitle = _subtitle)}>Nueva tarea</h2>
                <input
                    className='input form-control'
                    value={newTareaValue}
                    onChange={onChange}
                />
                <form onSubmit={onSubmit}>
                    <button className="btn btn-sm btn-light" onClick={closeModal}>Cerrar</button>&nbsp;
                    <button className="btn btn-sm btn-dark" type="submit">Agregar</button>
                </form>
            </Modal>
        </div>
    );
}

export { Boton };
