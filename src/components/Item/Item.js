import React from 'react'

function Item(props) {
    return (
        <li >
            <div className='item'>
                <p className=''>{props.text}</p>
                <button
                    type="button"
                    className={` buttons btn btn-primary ${props.completed && 'btn-success'}`}
                    onClick={props.onComplete}
                ><i className="material-icons">check</i>
                </button>

                <button
                    type="button"
                    className='buttons btn btn-danger'
                    onClick={props.onDelete}
                >
                    <i className="material-icons">close</i>
                </button>
            </div>
        </li>
    );

}

export { Item }