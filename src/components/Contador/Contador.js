import React from 'react'

function Contador({total, completed}) {
    return (
      <div>
          <h5><i className="material-icons">playlist_add_check</i> SE HAN COMPLETADO: {completed} DE {total} TAREAS</h5>
      </div>
    );
  }
  
  export { Contador };
  