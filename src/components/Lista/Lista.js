import React from 'react'

function Lista(props) {

    return (
        <ul className=''>
            {props.children}
        </ul>
    )
}

export { Lista }